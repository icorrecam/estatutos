
==== [#articulo-25]#Artículo 25.— ((Junta Electoral)) y ((Calendario))#

Concluido el mandato de la ((Junta Directiva)) o aprobada una ((moción de censura)), en el plazo de *_30 días_*, la persona que ejerza la ((Presidencia en funciones)), convocará ((elecciones)) y constituirá la ((Junta Electoral)), que estará formada por dos personas que, voluntariamente, se presten para esta función.

Estas personas no podrán formar parte de ninguna de las candidaturas presentadas.

En caso de no presentarse voluntarios, formarán la citada Junta los dos asociados de mayor y de menor edad.

Corresponde a la ((Junta Electoral)):

. Organizar las ((elecciones)), resolviendo sobre cualquier asunto que atañe a su desarrollo.
. Aprobar definitivamente el ((censo electoral)).
. Resolver las ((impugnaciones)) que se presenten en relación con el ((proceso electoral)).