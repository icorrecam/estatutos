
==== [#articulo-06]#Artículo 6.— ((Órganos sociales)).#

Son órganos de la asociación:

* La ((Asamblea General)).
* ((Junta Directiva)).

Adicionalmente, la Asociación podrá constituir en su seno órganos destinados a la promoción de sus fines, tales como «_((Comisiones)) de actividades_» o cualesquiera otros órganos de carácter consultivo.
