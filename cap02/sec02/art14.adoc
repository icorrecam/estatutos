
==== [#articulo-14]#Artículo 14.— Miembros del órgano de representación.#

Serán ((requisitos)) indispensables para ser miembro de la Junta Directiva:

. Ser miembro de la Asociación.
. Ser mayor de edad.
. Estar en pleno uso de los derechos civiles.
. No estar incurso en los motivos de incompatibilidad establecidos en la legislación vigente.
