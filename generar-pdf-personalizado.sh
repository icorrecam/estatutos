#!/usr/bin/env bash

fullfile="estatutos.adoc"

filename=$(basename -- "$fullfile")
filename="${filename%.*}"
extension="${filename##*.}"

TEMA="estatutos"

AUTOR="Junta Promotora"
COPYRIGHT="Junta Promotora"
CREATIONDATE=""
CREATOR="Creado usando «Asciidoctor»"
DESCRIPTION="Adaptación, como documento PDF, de los estatutos de la Asociación de Antiguos Alumnos y Personal de la Universidad Laboral de Las Palmas y del CIFP Felo Monzón Grau-Bassas."
KEYWORDS="Estatutos, Asociación, Normas"
MARKED=false
MODDATE=""
PDFVERSION=""
PRODUCER="Junta Promotora"
SUBJECT="Asociación de Antiguos Alumnos y Personal de la Universidad Laboral de Las Palmas y del CIFP Felo Monzón Grau-Bassas"
TITLE="Estatutos de la Asociación de Antiguos Alumnos y Personal de la Universidad Laboral de Las Palmas y del CIFP Felo Monzón Grau-Bassas"
TRAPPED="False"

# Ver: https://exiftool.org/TagNames/XMP.html#pdf

asciidoctor-pdf --trace -a pdf-theme="${TEMA}" "${fullfile}" && \
    exiftool \
        -Author="${AUTOR}" \
        -Copyright="${COPYRIGHT}" \
        -CreationDate="" \
        -Creator="${CREATOR}" \
        -Description="${DESCRIPTION}" \
        -Keywords="${KEYWORDS}" \
        -Marked="${MARKED}" \
        -ModDate="${MODDATE}" \
        -PDFVersion="${PDFVERSION}" \
        -Producer="${PRODUCER}" \
        -Subject="${SUBJECT}" \
        -Title="${TITLE}" \
        -Trapped="${TRAPPED}" \
        "${filename}".pdf && \
        okular "${filename}".pdf

