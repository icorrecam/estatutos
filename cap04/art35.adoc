
==== [#articulo-35]#Artículo 35.— Normas generales.#

En el ejercicio de la ((potestad disciplinaria)) se respetarán los siguientes principios generales:

* ((Proporcionalidad)) con la gravedad de la infracción, atendiendo a la naturaleza de los hechos, las ((consecuencias)) de la infracción y la ((concurrencia)) de circunstancias atenuantes o agravantes.
* Inexistencia de doble sanción por los mismos hechos.
* Aplicación de los ((efectos retroactivos)) favorables.
* Prohibición de sancionar por infracciones no tipificadas en el momento de su comisión.

La ((responsabilidad disciplinaria)) se extingue en todo caso por:

* El cumplimiento de la sanción.
* La prescripción de la infracción.
* La prescripción de la sanción.
* El fallecimiento del infractor.

Para la imposición de las correspondientes ((sanciones disciplinarias)) se tendrán en cuenta las ((circunstancias agravantes)) de la ((reincidencia)) y ((atenuante)) de ((arrepentimiento espontáneo)).

Hay reincidencia cuando la persona autora de la falta hubiese sido sancionada anteriormente por cualquier infracción de igual gravedad, o por dos o más que lo fueran de menor.

La reincidencia se entenderá producida en el transcurso de un año, contado a partir de la fecha en que se haya cometido la primera infracción.