# Estatutos

Repositorio con los estatutos de nuestra asociación.

## Como empezar…

En el caso de que quieras replicar este repositorio, o quieras hacer alguna modificación al mismo, en tu propia máquina, lo adecuado es que prepares el entorno de tu máquina con las herramientas adecuadas.

También puede ser oportuno que te abras una cuenta en este servicio, para que el histórico de tus actividades sea asociado a esa cuenta.

Si tu intención es editar este repositorio, lo normal es que sigas esta lógica de trabajo:

1. Prepares lo necesario en el equipo en el que vayas a trabajar.
2. Traes (clonas) una copia del repositorio a tu equipo.
3. Pruebas a editar los cambios que estimes oportunos, en tu equipo local, hasta que estés satisfecho.
4. Subes los cambios realizados en tu equipo local, al repositorio y los integras.
5. Etiquetas los cambios realizados, para tener un histórico mejor documentado.

Otra posibilidad es la de que quieras hacer un _fork_ (hacer una copia) de este repositorio y continuar trabajando a partir de tu propia copia. El único requisito es que te abras tu propia cuenta en este servicio. Más adelante podrás, si así lo decides, llevarte el repositorio a otros servicios similares que están disponibles públicamente.

El _flujo de trabajo_ sería muy parecido, pero el apartado 2 se dividiría en dos procesos:

_2a)_. Crearías tu propio _fork_, usando el botón que se puede ver en la parte alta de esta página y que aparece etiquetado con la palabra _fork_.
_2b_). Clonar tu propio _fork_ y descargarlo en tu equipo local.

A partir de ahí, el resto del _flujo de trabajo_ es el mismo.

## Preparación de tu entorno local

Comencemos preparando todo en tu equipo local.

### Herramientas recomendadas

Estas son las herramientas que se han utilizado durante el desarrollo de este repositorio. Seguir usando las mismas herramientas permitirá mantener la consistencia de todo el proyecto.

#### Git

Aunque te puedes descargar este repositorio como un único fichero comprimido, en diferentes formatos, la forma correcta de descargarlo es usando Git[^git].

Hay muchas formas de instalar Git en tu equipo local. Lo habitual es recomendar su instalación desde su [página oficial](https://git-scm.com/).

#### Asciidoctor

AsciiDoc es un lenguaje de escritura en texto plano que puede exportarse a formatos como HTML, PDF o ePub, entre otros.

Asciidoctor es un procesador que nos permite obtener, a partir de texto plano, escrito en AsciiDoc, documentación en varios formatos con un estilo más que aceptable y responsive “out of the box”. La sintaxis de AsciiDoc es sencilla y bien documentada, y sirve no sólo para artículos sino para libros y otros tipos de documentos, incluido, por supuesto, la generación de estos estatutos.

Para instalar AsciiDoctor en tu equipo local, te aconsejamos mirar, también, en su [página oficial](https://docs.asciidoctor.org/asciidoctor/latest/install/).

#### Un editor de textos

Nos vale cualquier editor de textos[^editor_de_textos], por básico que sea: _Notepad_, para Windows, _TextEdit_, para Mac y _Nano_, para sistemas Unix.

Para que no haya dudas, un editor de textos NO es lo mismo que un procesador de textos[^procesador_de_textos]. Por lo que no se pueden usar herramientas de alto nivel como pueden ser el _Word_, de MicrosoftOffice, el _Writer_, de Libreoffice o similares.

Un editor de texto que podemos recomendar, al ser multiplataforma, y que puedes usar para modificar este repositorio, es el [Visual Studio Code](https://code.visualstudio.com/)

### Clonado del repositorio

Clonar un repositorio es, simplemente, traerte una copia del repositorio, a tu equipo local.

En el caso de que hayas hecho un _fork_ de este repositorio, sólo tendrás que modificar el dato correspondiente a la dirección de tu propio repositorio.

Si lo que quieres es clonar este repositorio, y teniendo _Git_ instalado y funcionando, estas son las instrucciones:

```sh
git clone https://gitlab.com/icorrecam/estatutos.git
```

Este comando creará una carpeta llamada `estatutos` que contendrá tanto los ficheros contenidos en el repositorio, como el historial de todos los cambios realizados hasta ese momento.

A partir de este momento, ya es perfectamente factible comenzar a modificar cualquiera de los ficheros que forman este repositorio.

## Edición del contenido de este repositorio

El primer paso consiste en entrar en la carpeta que acabamos de crear: `estatutos`

A partir de aquí, podremos editar cualquier fichero que esté en esta o en cualquiera de las carpetas del proyecto.

El _flujo de trabajo_ básico podría ser el siguiente:

1. Edito un fichero y realizo los cambios que estime oportunos. Luego lo grabo…
2. Desde una consola ejecuto `./generar-pdf.sh` para visualizar los cambios…
3. Vuelvo al punto 1.

Este _flujo de trabajo_ lo repetiremos tantas veces como haga falta, en todos los ficheros que queramos modificar.

__IMPORTANTE__: Como última anotación, con respecto a la fase de edición, decir que el punto de entrada a toda la documentación, donde se organiza y donde se definen todos los parámetros básicos, es el fichero `estatutos.adoc`.

## Sincronización con el repositorio

Una vez que hemos terminado la fase de edición, y estamos contentos con el resultado, lo recomendable es volver a actualizar el repositorio original con los cambios que hayamos realizado en nuestro equipo local, dejando todo preparado por si hubiera que editar el contenido del repositorio en un futuro,

Los pasos que detallaré a continuación son una simplificación del proceso, pero siguen siendo perfectamente válidos. Si tienes conocimientos del uso de Git, podrás sincronizar todo siguiendo un proceso más detallado.

Para sincronizar los cambios que hemos realizado en nuestro equipo local, seguiremos estos pasos:

```sh
$ Primero nos aseguramos de donde estamos.
$ Deberíamos estar en la raíz de la carpeta estatutos.
pwd

$ Ahora revisamos y añadimos posibles nuevos archivos:
git add .

$ El siguiente paso es preparar todos los cambios
$ antes de subirlos al repositorio original.
$ Siempre conviene que los cambios vayan acompañados
$ de un comentario que ayude a entender lo que se ha hecho.
$ No copies y pegues sin cambiar el mensaje de ejemplo.
git commit -m "MENSAJE CON RESUMEN DE LOS CAMBIOS" -a

$ El último paso, ahora si, es el de  sincronizar nuestro
$ cambios con el repositorio original:
git push

$ Si todo ha ido bien, ahora hemos vuelto a sincronizar
$ todos nuestros cambios correctamente.
```

## Estado del proyecto

Este proyecto va a quedar etiquetado con su versión 1, cuando estos estatutos sean aprobados por el organismo público adecuado y nos den y asignen un número de registro para nuestra asociación.

Si más adelante, como en todas las asociaciones, se quiere modificar el contenido de estos estatutos, tanto el sistema de etiquetas, como el mismo histórico del sistema permitirá identificar hasta el más mínimo cambio realizado en cualquiera de los ficheros, permitiendo, además, una rápida comparación visual para facilitar ese tipo de búsquedas.

## Autores y reconocimiento

Estos estatutos han sido realizados gracias a la colaboración de una serie de personas, que han dedicado tiempo personal a tratar de sacar adelante esta asociación.

Han participado en la fase de elaboración de estos estatutos:

- Begoña
- Canino
- Dulce Auxiliadora Déniz
- Isaac Godoy
- Isabel Medina
- Javier Santana
- Luis Cabrera

El autor de esta documentación así como de este repositorio, es Luis Cabrera Saúco <estatutos.lcabrera@erine.email>.

Asimismo, tenemos que mencionar que la imagen que usamos en la portada, [gp23](https://freesvg.org/gp23), se encuentra bajo «dominio público», tal como comentan en su [página de descarga](https://freesvg.org/gp23).

## Licencia

Esta obra está cubierta por la siguiente licencia:

CC0 1.0 Deed
CC0 1.0 Universal

[Más información…](https://creativecommons.org/publicdomain/zero/1.0/)

Básicamente, esta obra queda en el dominio público.

### English

No Copyright

The person who associated a work with this deed has dedicated the work to the public domain by waiving all of his or her rights to the work worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.

You can copy, modify, distribute and perform the work, even for commercial purposes, all without asking permission.

### Español

Sin derechos autorales

La persona que asoció una obra con este resumen ha dedicado la obra al dominio público, mediante la renuncia a todos sus derechos a la obra bajo las leyes de derechos autorales en todo el mundo, incluyendo todos los derechos conexos y afines, en la medida permitida por la ley.

Puede copiar, modificar, distribuir e interpretar la obra, incluso para propósitos comerciales, sin pedir permiso.

----

[^git]: Descripción en la wikipedia de lo que es [Git](https://es.wikipedia.org/wiki/Git)
[^editor_de_textos]: Explicación de los que es un [Editor de texto](https://es.wikipedia.org/wiki/Editor_de_texto)
[^procesador_de_textos]: Explicación de los que son los [Procesadores de texto](https://es.wikipedia.org/wiki/Procesador_de_texto)
