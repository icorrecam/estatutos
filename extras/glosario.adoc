[glossary]
== Glosario

[IMPORTANT]
====
Por terminar…
====

Mayoría simple:: Una mayoría simple es aquella que obtiene más votos que la minoría, pero no alcanza el 50% de los votos emitidos.
Mayoría absoluta:: Una mayoría absoluta es aquella que obtiene más del 50% de los votos emitidos. +
+
Para que una votación se considere aprobada por mayoría absoluta, es necesario que la opción que obtenga más votos obtenga más de la mitad de los votos emitidos, sin contar las abstenciones.

<<<
