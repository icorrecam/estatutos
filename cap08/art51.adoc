
==== [#articulo-51]#Artículo 51.— Causas.#

La Asociación puede disolverse:

* Por Sentencia judicial firme.
* Por acuerdo de la Asamblea General Extraordinaria.
* Por las causas determinadas en el https://www.conceptosjuridicos.com/codigo-civil-articulo-39/[artículo 39 del Código Civil.].