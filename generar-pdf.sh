#!/usr/bin/env bash

fullfile="estatutos.adoc"

filename=$(basename -- "$fullfile")
filename="${filename%.*}"
extension="${filename##*.}"

TEMA="estatutos"

asciidoctor -a allow-uri-read "${fullfile}" && \
    asciidoctor-pdf -a allow-uri-read -a pdf-theme="${TEMA}" "${fullfile}" && \
    firefox-devedition "${filename}".html && \
    okular ${filename}.pdf

