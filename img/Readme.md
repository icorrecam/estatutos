# Imágenes

## Fondos de pantalla SVG

- [Fondos de pantalla A4](https://nohat.cc/s/svg%20border%20a4%20size)
  - [https://nohat.cc/f/svg-border-88-a4-size/freesvgorg123831-202109201222.html](https://nohat.cc/f/)svg-border-88-a4-size/freesvgorg123831-202109201222.html
  - [https://freesvg.org/1528296912](https://freesvg.org/1528296912)
  - …
- [FreeSVG](https://freesvg.org/1528306604)
  - [https://freesvg.org/random-binary-numbers](https://freesvg.org/random-binary-numbers)
  - [https://freesvg.org/creepy-glow-swamp](https://freesvg.org/creepy-glow-swamp)
  - [https://freesvg.org/gp23](https://freesvg.org/gp23)
  - [https://freesvg.org/draft-business-stamp-vector](https://freesvg.org/draft-business-stamp-vector)
  - …
